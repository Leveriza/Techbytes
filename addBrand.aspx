﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.master" AutoEventWireup="true" CodeFile="addBrand.aspx.cs" Inherits="addBrand" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="form-horizontal">
        <h4>Add Brand</h4>
        <hr />
        <div class="form-group">
            <asp:Label ID="lbl1" runat="server" CssClass="col-md-2 control-label" Text="Brand Name"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox ID="txtBrandName" CssClass="form-control" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="alert-danger" ErrorMessage="Brand Name is Empty" ControlToValidate="txtBrandName"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Button ID="Add" CssClass="btn btn-success btn-group-lg" runat="server" Text="Add Brand" OnClick="Add_Click" />
        </div>
        <div class="form-group">
            <asp:Label ID="Success" CssClass="text-success" runat="server" Text=""></asp:Label>
        </div>
    </div>
    <asp:datalist runat="server" DataKeyField="BrandID" DataSourceID="SqlDataSource1">
        <ItemTemplate>
            BrandID:
            <asp:Label ID="BrandIDLabel" runat="server" Text='<%# Eval("BrandID") %>' />
            <br />
            Name:
            <asp:Label ID="NameLabel" runat="server" Text='<%# Eval("Name") %>' />
            <br />
<br />
        </ItemTemplate>
    </asp:datalist>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Brands]"></asp:SqlDataSource>
</asp:Content>

