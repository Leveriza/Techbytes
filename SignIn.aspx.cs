﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public partial class SignIn : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack){
            if (Request.Cookies["UNAME"] != null && Response.Cookies["PASS"] != null)
            {
                UserName.Text = Request.Cookies["UNAME"].Value;
                Password.Text = Request.Cookies["PASS"].Value;
                CheckBox1.Checked = true;

            }
        }

    }

    protected void Login_Click(object sender, EventArgs e)
    {
        String CS = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        using (SqlConnection con = new SqlConnection(CS))
        {
            SqlCommand cmd = new SqlCommand("select * from Users where Username='"+UserName.Text+"' and Password='"+Password.Text+"'", con);
            con.Open();
            SqlDataAdapter adapt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapt.Fill(dt);
            if (dt.Rows.Count != 0){
                if (CheckBox1.Checked){
                    Response.Cookies["UNAME"].Value = UserName.Text;
                    Response.Cookies["PASS"].Value = Password.Text;

                    Response.Cookies["UNAME"].Expires = DateTime.Now.AddDays(3);
                    Response.Cookies["PASS"].Expires = DateTime.Now.AddDays(3);

                } else
                {
                    Response.Cookies["UNAME"].Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies["PASS"].Expires = DateTime.Now.AddDays(-1);
                }
                String userType = dt.Rows[0][6].ToString().Trim();
                Session["USERNAME"] = UserName.Text;

                if (userType == "1")
                {
                    //User is Admin
                    Response.Redirect("AdminPage.aspx");

                }
                else
                {
                    //User is not Admin
                    Response.Redirect("UserPage.aspx");

                }

            } else
            {
                lblError.Text = "Error! No User or Wrong Password.";
            }
        }
    }
}