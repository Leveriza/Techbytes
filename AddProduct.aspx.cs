﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.IO;
public partial class AddProduct : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Add_Click(object sender, EventArgs e)
    {
        String CS = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        String PNAME = "", PID = "";
        Success.Text = "";

        using (SqlConnection con = new SqlConnection(CS))
        {
            if (FileUploadPImage.HasFile)
            {
            SqlCommand cmd = new SqlCommand("insert into Products values('" + txtPName.Text + "','" + txtPPrice.Text + "','" + txtProductDesc.Text + "','" + txtPQuantity.Text + "','" + (ddlBrands.SelectedIndex + 1) + "','" + (ddlPCategory.SelectedIndex + 1) + "')", con);
            SqlCommand cmd2 = new SqlCommand("SELECT TOP 1 PID FROM Products ORDER BY PID DESC", con);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            PNAME = txtPName.Text.ToString();
            con.Open();
            PID = cmd2.ExecuteScalar().ToString();
            con.Close();
                string path = Server.MapPath("~/images/products/") + PID;

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                string Extension = Path.GetExtension(FileUploadPImage.PostedFile.FileName);
                SqlCommand cmd3 = new SqlCommand("insert into ProductImages values('" + PID + "','" + PNAME + "','" + Extension.ToString() + "')", con);
                con.Open();
                cmd3.ExecuteNonQuery();
                con.Close();
                FileUploadPImage.SaveAs(path + "\\" + PNAME + Extension.ToString());
                Success.Text = "Succcessfully Added Product: " + txtPName.Text + "!";
                txtPName.Text = string.Empty;
                txtPQuantity.Text = string.Empty;
                txtProductDesc.Text = string.Empty;
                txtPPrice.Text = string.Empty;
            }
        }
    }
}