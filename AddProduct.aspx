﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.master" AutoEventWireup="true" CodeFile="AddProduct.aspx.cs" Inherits="AddProduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="form-horizontal">
        <h4>Add Product</h4>
        <hr />
        <div class="form-group">
            <asp:Label ID="Label5" runat="server" CssClass="col-md-2 control-label" Text="Product Image"></asp:Label>
            <div class="col-md-3">
                <asp:FileUpload ID="FileUploadPImage" CssClass="form-control" runat="server" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" CssClass="alert-danger" ErrorMessage="Product Image is Empty" ControlToValidate="FileUploadPImage"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="lbl1" runat="server" CssClass="col-md-2 control-label" Text="Product Name"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox ID="txtPName" CssClass="form-control" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="alert-danger" ErrorMessage="Product Name is Empty" ControlToValidate="txtPName"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label6" runat="server" CssClass="col-md-2 control-label" Text="Product Brand"></asp:Label>
            <div class="col-md-3">
                <asp:DropDownList ID="ddlBrands" CssClass="form-control" runat="server" DataSourceID="SqlDataSource2" DataTextField="Name" DataValueField="Name"></asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT [BrandID], [Name] FROM [Brands]"></asp:SqlDataSource>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" CssClass="alert-danger" ErrorMessage="Brand is Empty" InitialValue="0" ControlToValidate="ddlBrands"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label1" runat="server" CssClass="col-md-2 control-label" Text="Product Quantity"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox ID="txtPQuantity" CssClass="form-control" runat="server" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" CssClass="alert-danger" ErrorMessage="Quantity is Empty" ControlToValidate="txtPQuantity"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label3" runat="server" CssClass="col-md-2 control-label" Text="Product Category"></asp:Label>
            <div class="col-md-3">
                <asp:DropDownList ID="ddlPCategory" CssClass="form-control" runat="server" DataSourceID="SqlDataSource1" DataTextField="Name" DataValueField="Name"></asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT [Name] FROM [Categories]"></asp:SqlDataSource>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" CssClass="alert-danger" ErrorMessage="Category is Empty" InitialValue="0" ControlToValidate="ddlPCategory"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label2" runat="server" CssClass="col-md-2 control-label" Text="Product Price"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox ID="txtPPrice" CssClass="form-control" runat="server" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" CssClass="alert-danger" ErrorMessage="Product Price is Empty" ControlToValidate="txtPPrice"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="Label4" runat="server" CssClass="col-md-2 control-label" Text="Product Description"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox ID="txtProductDesc" TextMode="MultiLine" CssClass="form-control" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" CssClass="alert-danger" ErrorMessage="Description is Empty" ControlToValidate="txtProductDesc"></asp:RequiredFieldValidator>
            </div>
        </div>

        <div class="form-group">
            <asp:Button ID="Add" CssClass="btn btn-success btn-group-lg" runat="server" Text="Add Product" OnClick="Add_Click" />
        </div>
                <div class="form-group">
            <asp:Label ID="Success" CssClass="text-success" runat="server" Text=""></asp:Label>
        </div>


    </div>
</asp:Content>

