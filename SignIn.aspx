﻿<%@ Page Title="Sign In" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="SignIn.aspx.cs" Inherits="SignIn" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
                    <!-- Start sign-ip -->
                <h3>Login</h3>
                <hr />
                <h4>Admin Username and Password</h4>
                <p>"admin"</p>
                <div class="form-horizontal">
                    <div class="form-group">
                        <asp:Label ID="lbl1" runat="server" CssClass="col-md-2 control-label" Text="Username"></asp:Label>
                        <div class="col-md-3">
                            <asp:TextBox ID="UserName" CssClass="form-control" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="alert-danger" ErrorMessage="Username is Empty" ControlToValidate="UserName"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label ID="Label1" runat="server" CssClass="col-md-2 control-label" Text="Password"></asp:Label>
                        <div class="col-md-3">
                            <asp:TextBox ID="Password" CssClass="form-control" runat="server" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" CssClass="alert-danger" ErrorMessage="Password is Empty" ControlToValidate="Password"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <asp:Label ID="Label2" runat="server" CssClass="control-label" Text="Remember me?"></asp:Label>
                            <asp:CheckBox ID="CheckBox1" runat="server" />
                            <br>
                            <asp:Button ID="Login" runat="server" Text="Login" CssClass="btn btn-primary btn-group-md btn-default" OnClick="Login_Click"></asp:Button>
                            <a href="SignUp.aspx" class="btn btn-primary">Sign-Up</a>
                        </div>
                    </div>
                </div>
    <div class="form-group">
        <div class="col-md-2"></div>
        <div class="col-md-6">
            <asp:Label ID="lblError" runat="server" CssClass="text-danger" Text=""></asp:Label>
        </div>
    </div>
</asp:Content>



