﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="Contact.aspx.cs" Inherits="Contact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Members
                </th>
                <th>Contact #</th>
                <th>Email
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Ephraim Reiner D. Leveriza</td>
                <td>09175103111</td>
                <td>ephraim@leveriza.net</td>
            </tr>
            <tr>
                <td>Alexander Pascual</td>
                <td>-----------</td>
                <td>-----------</td>
            </tr>
            <tr>
                <td>Suzy Juego Relativo</td>
                <td>-----------</td>
                <td>-----------</td>
            </tr>
        </tbody>
        <p><a class="btn btn-primary btn-lg" href="https://gitlab.com/Leveriza/techbytes" role="button">GitLab Repository</a></p>
    </table>
</asp:Content>

