﻿<%@ Page Title="Sign Un" Language="C#" MasterPageFile="~/Default.master" AutoEventWireup="true" CodeFile="SignUp.aspx.cs" Inherits="SignUp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <!-- Start sign-up -->
        <h3>Register</h3>
        <hr />
        <div class="form-horizontal">
            <div class="form-group">
                <label>Username:</label>
                <asp:TextBox ID="tbUsername" CssClass="form-control" runat="server" placeholder="eleveriza"></asp:TextBox>
            </div>
            <div class="form-group">
                <label>Password:</label>
                <asp:TextBox ID="tbPassword" CssClass="form-control" runat="server" placeholder="Not 123456" TextMode="Password"></asp:TextBox>
            </div>
            <div class="form-group">
                <label>Confirm Password:</label>
                <asp:TextBox ID="tbCPassword" CssClass="form-control" runat="server" placeholder="Not 123456" TextMode="Password"></asp:TextBox>
            </div>
            <div class="form-group">
                <label>E-mail Address:</label>
                <asp:TextBox ID="tbEmail" CssClass="form-control" runat="server" placeholder="ephraim@leveriza.net" TextMode="Email"></asp:TextBox>
            </div>
            <div class="form-group">

                <label class="col-xs-11">Fullname:</label>
                <asp:TextBox ID="tbName" CssClass="form-control" runat="server" placeholder="Ephraim Reiner D. Leveriza"></asp:TextBox>
            </div>
            <div class="form-group">
                <label class="col-xs-11">Address:</label>
                <asp:TextBox ID="tbAddress" CssClass="form-control" runat="server" placeholder="Dasma, Cavite"></asp:TextBox>
            </div>
            <asp:Button ID="btnSignUp" runat="server" CssClass="btn btn-primary" Text="Register" OnClick="btnSignUp_Click" />
            <a href="SignIn.aspx" class="btn btn-primary">Sign-In</a>
            <asp:Label ID="lblMsg" runat="server"></asp:Label>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="tbPassword" ControlToValidate="tbCPassword" ErrorMessage="Password is not the same"></asp:CompareValidator>
        </div>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Users]"></asp:SqlDataSource>
</asp:Content>
