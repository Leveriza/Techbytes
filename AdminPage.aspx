﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.master" AutoEventWireup="true" CodeFile="AdminPage.aspx.cs" Inherits="AdminPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1>Welcome to Techbyt.es!</h1>
    <p>Where Tech bytes you and it will Hertz</p>
    <br />
    <h2>Products</h2>
                <div class="row">
    <asp:repeater id="Repeater1" runat="server" datasourceid="SqlDataSource1">
        <ItemTemplate>
            <!-- Product -->
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="thumbnail">
                      <img src="images/products/<%#Eval("PID") %>/<%#Eval("PName") %>.jpg" />
                        <div class="caption">
                            <h3><%#Eval("PName") %></h3>
                            <p><%#Eval("PDesc") %></p>
                             <p>Qty: <%#Eval("PQty") %></p>
                              <p>Price: <%#Eval("PPrice") %></p>
                            <p><a href="#" class="btn btn-success" role="button">Update</a><a href="#" class="btn btn-danger" role="button">Delete</a></p>
                        </div>
                    </div>
                </div>
            <!-- end of product -->
        </ItemTemplate>
    </asp:repeater>
                </div>
    <asp:sqldatasource id="SqlDataSource1" runat="server" connectionstring="<%$ ConnectionStrings:ConnectionString %>" selectcommand="SELECT * FROM [Products]"></asp:sqldatasource>
</asp:Content>

