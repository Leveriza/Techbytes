﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.master" AutoEventWireup="true" CodeFile="AddCategory.aspx.cs" Inherits="AddCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div class="form-horizontal">
        <h4>Add Category</h4>
        <hr />
        <div class="form-group">
            <asp:Label ID="lbl1" runat="server" CssClass="col-md-2 control-label" Text="Category Name"></asp:Label>
            <div class="col-md-3">
                <asp:TextBox ID="txtCategoryName" CssClass="form-control" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="alert-danger" ErrorMessage="Category Name is Empty" ControlToValidate="txtCategoryName"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-group">
            <asp:Button ID="Add" CssClass="btn btn-success btn-group-lg" runat="server" Text="Add Category" OnClick="Add_Click" />
        </div>
        <div class="form-group">
            <asp:Label ID="Success" CssClass="text-success" runat="server" Text=""></asp:Label>
        </div>
    </div>
</asp:Content>

